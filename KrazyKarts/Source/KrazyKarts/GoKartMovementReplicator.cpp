// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKartMovementReplicator.h"

#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/GameStateBase.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UGoKartMovementReplicator::UGoKartMovementReplicator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicated(true);
}

void UGoKartMovementReplicator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> &OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGoKartMovementReplicator, ServerState)
}

// Called when the game starts
void UGoKartMovementReplicator::BeginPlay()
{
	Super::BeginPlay();

	if (!ensure(GetOwner()))
	{
		FGenericPlatformMisc::RequestExit(false);
	}

	if (GetOwner()->HasAuthority())
	{
		GetOwner()->NetUpdateFrequency = 1.F;
	}

	MovementComponent = GetOwner()->FindComponentByClass<UGoKartMovementComponent>();
	USceneComponent *Box = GetOwner()->FindComponentByClass<USceneComponent>();
	MeshOffsetRoot = Box->GetChildComponent(0);

	RoleString = UEnum::GetValueAsString(TEXT("Engine.ENetRole"), GetOwnerRole());
}

// Called every frame
void UGoKartMovementReplicator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const FGoKartMove &LastMove = MovementComponent->LastMove;

	switch (GetOwnerRole())
	{
	case ROLE_Authority:
		if (GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy)
		{
			UpdateServerState(LastMove);
		}
		break;

	case ROLE_AutonomousProxy:
		UnacknowledgedMoves.Add(LastMove);
		ServerSendMove(LastMove);
		break;

	case ROLE_SimulatedProxy:
		ClientTick(DeltaTime);
		break;

	default:
		break;
	}

	DrawDebugString(GetWorld(), FVector{0.F, 0.F, 100.F}, RoleString, GetOwner(), FColor::White, DeltaTime);
}

void UGoKartMovementReplicator::ClientTick(float DeltaTime)
{
	ClientTimeSinceUpdate += DeltaTime;

	if (ClientTimeBetweenLastUpdates > KINDA_SMALL_NUMBER && MovementComponent)
	{
		float Alpha = ClientTimeSinceUpdate / ClientTimeBetweenLastUpdates;

		FHermiteCubicSpline Spline = CreateSpline();
		InterpolateLocation(Spline, Alpha);
		InterpolateVelocity(Spline, Alpha);
		InterpolateRotation(Alpha);
	}
}

FHermiteCubicSpline UGoKartMovementReplicator::CreateSpline()
{
	FHermiteCubicSpline Spline;

	Spline.StartLocation = ClientStartTransform.GetLocation();
	Spline.TargetLocation = ServerState.Transform.GetLocation();
	Spline.StartDerivative = ClientStartVelocity * VelocityToDerivative();
	Spline.TargetDerivative = ServerState.Velocity * VelocityToDerivative();

	return Spline;
}

void UGoKartMovementReplicator::InterpolateLocation(const FHermiteCubicSpline &Spline, const float Alpha)
{
	if (MeshOffsetRoot)
	{
		const FVector &NewLocation = Spline.InterpolateLocation(Alpha);
		MeshOffsetRoot->SetWorldLocation(NewLocation);
	}
}

void UGoKartMovementReplicator::InterpolateVelocity(const FHermiteCubicSpline &Spline, const float Alpha)
{
	const FVector &NewDerivative = Spline.InterpolateDerivative(Alpha);
	const FVector &NewVelocity = NewDerivative / VelocityToDerivative();
	MovementComponent->Velocity = NewVelocity;
}

void UGoKartMovementReplicator::InterpolateRotation(const float Alpha)
{
	if (MeshOffsetRoot)
	{
		const FQuat &TargetRotation = ServerState.Transform.GetRotation();
		const FQuat &StartRotation = ClientStartTransform.GetRotation();
		const FQuat &NewRotation = FQuat::Slerp(StartRotation, TargetRotation, Alpha);
		MeshOffsetRoot->SetWorldRotation(NewRotation);
	}
}

bool UGoKartMovementReplicator::ServerSendMove_Validate(const FGoKartMove &Move)
{
	float ProposedTime = ClientSimulatedTime + Move.DeltaTime;
	bool ClientRunningAhead = ProposedTime > GetWorld()->GetGameState()->GetServerWorldTimeSeconds();

	if (ClientRunningAhead)
	{
		UE_LOG(LogTemp, Error, TEXT("Client is running too fast."))
	}
	else if (!Move.IsValid())
	{
		UE_LOG(LogTemp, Error, TEXT("Received invalid move."))
	}

	return Move.IsValid() && !ClientRunningAhead;
}

void UGoKartMovementReplicator::ServerSendMove_Implementation(const FGoKartMove &Move)
{
	// Though movement component seems to be valid here, a check is not harmful
	if (MovementComponent)
	{
		MovementComponent->SimulateMove(Move);
		ClientSimulatedTime += Move.DeltaTime;
		UpdateServerState(Move);
	}
}

void UGoKartMovementReplicator::UpdateServerState(const FGoKartMove &Move)
{
	ServerState.LastMove = Move;
	ServerState.Transform = GetOwner()->GetActorTransform();
	ServerState.Velocity = MovementComponent->Velocity;
}

void UGoKartMovementReplicator::ClearAcknowledgedMoves(const FGoKartMove &LastMove)
{
	UnacknowledgedMoves.RemoveAll([&LastMove](const FGoKartMove &Move) {
		// Remove all moves older than the last move == preserve all moves newer than last move
		return !(Move.Time > LastMove.Time);
	});
}

void UGoKartMovementReplicator::OnRep_ServerState()
{
	switch (GetOwnerRole())
	{
	case ROLE_SimulatedProxy:
		OnRep_ServerState_SimulatedProxy();
		break;

	case ROLE_AutonomousProxy:
		OnRep_ServerState_AutonomousProxy();
		break;

	default:
		break;
	}
}

void UGoKartMovementReplicator::OnRep_ServerState_SimulatedProxy()
{
	if (MovementComponent)
	{
		ClientTimeBetweenLastUpdates = ClientTimeSinceUpdate;

		// We just updated client time! Reset to zero:
		ClientTimeSinceUpdate = 0.F;

		if (MeshOffsetRoot)
		{
			ClientStartTransform.SetLocation(MeshOffsetRoot->GetComponentLocation());
			ClientStartTransform.SetRotation(MeshOffsetRoot->GetComponentQuat());
			ClientStartVelocity = MovementComponent->Velocity;
		}

		GetOwner()->SetActorTransform(ServerState.Transform);
	}
}

void UGoKartMovementReplicator::OnRep_ServerState_AutonomousProxy()
{
	if (MovementComponent)
	{
		GetOwner()->SetActorTransform(ServerState.Transform);
		MovementComponent->Velocity = ServerState.Velocity;

		ClearAcknowledgedMoves(ServerState.LastMove);

		for (const FGoKartMove &Move : UnacknowledgedMoves)
		{
			MovementComponent->SimulateMove(Move);
		}
	}
}
