// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKartMovementComponent.h"

#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/GameStateBase.h"


// Sets default values for this component's properties
UGoKartMovementComponent::UGoKartMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!ensure(GetOwner()))
	{
		FGenericPlatformMisc::RequestExit(false);
	}

	// Can't be static or const but IS a const.
	SMALL_G = -GetWorld()->GetGravityZ() / CM_TO_M;
}


void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() == ROLE_AutonomousProxy || GetOwner()->GetRemoteRole() == ROLE_SimulatedProxy)
	{
		SimulateMove(LastMove = CreateMove(DeltaTime));
	}
}


FGoKartMove UGoKartMovementComponent::CreateMove(float DeltaTime)
{
	FGoKartMove Move;

	Move.DeltaTime = DeltaTime;
	Move.SteeringThrow = SteeringThrow;
	Move.Throttle = Throttle;
	Move.Time = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();

	return Move;
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move)
{
	FVector Force = MaxDrivingForce * Move.Throttle * GetOwner()->GetActorForwardVector();
	Force += GetAirResistance() + GetRollingResistance();

	FVector Acceleration = Force / Mass;

	// Compensate for change in velocity - dv = a * dt
	Velocity += Acceleration * Move.DeltaTime;

	ApplyRotation(Move.DeltaTime, Move.SteeringThrow);
	UpdateLocationFromVelocity(Move.DeltaTime);
}

FVector UGoKartMovementComponent::GetAirResistance()
{
	return DragCoefficient * Velocity.SizeSquared() * -Velocity.GetSafeNormal();
}

FVector UGoKartMovementComponent::GetRollingResistance()
{
	float NormalForce = Mass * SMALL_G;
	return RollingResistanceCoefficient * NormalForce * -Velocity.GetSafeNormal();
}

void UGoKartMovementComponent::ApplyRotation(float DeltaTime, float InSteeringThrow)
{
	float DeltaTranslation = FVector::DotProduct(GetOwner()->GetActorForwardVector(), Velocity) * DeltaTime;
	float RotationAngle = DeltaTranslation / MinTurningRadius * InSteeringThrow;
	FQuat RotationDelta{ GetOwner()->GetActorUpVector(), RotationAngle };

	// Let the velocity point in the direction of rotation
	Velocity = RotationDelta.RotateVector(Velocity);
	GetOwner()->AddActorWorldRotation(RotationDelta);
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(float DeltaTime)
{
	FVector DeltaTranslation = Velocity * DeltaTime * CM_TO_M;

	FHitResult Hit;
	GetOwner()->AddActorWorldOffset(DeltaTranslation, true, &Hit);

	// Stop the GoKart if it collides
	if (Hit.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}
